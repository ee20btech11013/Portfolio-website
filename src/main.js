import Vue from "vue";
import App from "./App.vue";
import AOS from "aos";
import VueAwesomeSwiper from "vue-awesome-swiper";
import "aos/dist/aos.css";
import vuetify from "./plugins/vuetify";
import VueTyperPlugin from "vue-typer";
import PortalVue from "portal-vue";
import './registerServiceWorker'
// import Scrollspy from "vue2-scrollspy";
// // use default options
// Vue.use(Scrollspy);
// Vue.config.productionTip = false;

Vue.use(VueTyperPlugin, VueAwesomeSwiper);
Vue.use(PortalVue);
new Vue({
  vuetify,
  // PortalVue,
  el: "#app",
  render: (h) => h(App),
  mounted() {
    AOS.init({
      duration: 1000,
      once: true,
    });
  },
}).$mount("#app");
